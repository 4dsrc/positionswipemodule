﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace PositionSwipeModuleCSharp
{
    class PositionSwipeModuleCSharp
    {
        public PositionSwipeModuleCSharp()
        {
            m_PositionSwipeMgr = PositionSwipeGenerator();
        }

        ~PositionSwipeModuleCSharp()
        {
            int nRet = DeletePositionSwipeMgr();
        }

        public int AddPositionSwipeData(
            int nChannel, string strDscID,
            int nWidth, int nHeight, int nPositionSwipeLength,
            double dMoveX, double dMoveY, double dRotateX, double dRotateY, double dAngle, double dScale,
            int nMarginX, int nMarginY, int nMarginWidth, int nMarginHeight,
            int nX1, int nY1, int nX2, int nY2, int nX3, int nY3, int nX4, int nY4)
        {
            char[] szDscID = strDscID.ToCharArray();
            return AddPositionSwipeDataCSharp(
                m_PositionSwipeMgr,
                nChannel, szDscID, nWidth, nHeight, nPositionSwipeLength,
                dMoveX, dMoveY, dRotateX, dRotateY, dAngle, dScale,
                nMarginX, nMarginY, nMarginWidth, nMarginHeight,
                nX1, nY1, nX2, nY2, nX3, nY3, nX4, nY4);
        }

        public int ApplyPositionSwipeData()
        {
            return ApplyPositionSwipeDataCSharp(m_PositionSwipeMgr);
        }

        public int SetVecPositionAxis(int nChannel, int nZoomRatio, int nPosX, int nPosY)
        {
            return SetVecPositionAxisCSharp(m_PositionSwipeMgr, nChannel, nZoomRatio, nPosX, nPosY);
        }

        public int GetMovedAxis(int nChannel, ref int nZoomRatio, ref int nPosX, ref int nPosY)
        {
            return GetMovedAxisCSharp(m_PositionSwipeMgr, nChannel, ref nZoomRatio, ref nPosX, ref nPosY);
        }

        public int GetSEIPositionSwipeData(out byte[] arrMultiviewData, out int nMultiviewDataSize)
        {
            IntPtr szMultiviewData;
            int nRet = GetPositionSwipeDataForSEICSharp(m_PositionSwipeMgr, out szMultiviewData, out nMultiviewDataSize);

            arrMultiviewData = new byte[nMultiviewDataSize];
            Marshal.Copy(szMultiviewData, arrMultiviewData, 0, nMultiviewDataSize);

            return nRet;
        }

        public int GetSEIMultiviewDataByChannel(
            int nChannel, int nMainViewChannel, out byte[] arrMultiviewData, out int nMultiviewDataSize)
        {
            IntPtr szMultiviewData;
            int nRet = GetSEIMultiviewDataByChannelCSharp(m_PositionSwipeMgr, nChannel, nMainViewChannel, out szMultiviewData, out nMultiviewDataSize);
            
            arrMultiviewData = new byte[nMultiviewDataSize];
            Marshal.Copy(szMultiviewData, arrMultiviewData, 0, nMultiviewDataSize);

            return nRet;
        }

        private IntPtr PositionSwipeGenerator()
        {
            return CreatePositionSwipeMgrCSharp();
        }
        private int DeletePositionSwipeMgr()
        {
            return DeletePositionSwipeMgrCSharp(m_PositionSwipeMgr);
        }

        private IntPtr m_PositionSwipeMgr;

        [DllImport("PositionSwipeModule.dll")] extern private static IntPtr CreatePositionSwipeMgrCSharp();
        [DllImport("PositionSwipeModule.dll")] extern private static int DeletePositionSwipeMgrCSharp(IntPtr pPositionSwipeMgr);

        [DllImport("PositionSwipeModule.dll")] extern private static int AddPositionSwipeDataCSharp(
           IntPtr pPositionSwipeMgr,
           int nChannel, char[] strDscID, int nWidth, int nHeight, int nPositionSwipeLength,
           double dMoveX, double dMoveY, double dRotateX, double dRotateY, double dAngle, double dScale,
           int nMarginX, int nMarginY, int nMarginWidth, int nMarginHeight,
           int nX1, int nY1, int nX2, int nY2, int nX3, int nY3, int nX4, int nY4);

        [DllImport("PositionSwipeModule.dll")] extern private static int ApplyPositionSwipeDataCSharp(IntPtr positionSwipeMgr);

        [DllImport("PositionSwipeModule.dll")] extern private static int SetVecPositionAxisCSharp(
            IntPtr positionSwipeMgr, int nChannel, int nZoomRatio, int nPosX, int nPosY);

        [DllImport("PositionSwipeModule.dll")] extern private static int GetMovedAxisCSharp(
            IntPtr positionSwipeMgr, int nChannel, ref int nZoomRatio, ref int nPosX, ref int nPosY);

        [DllImport("PositionSwipeModule.dll")] extern private static int GetPositionSwipeDataForSEICSharp(
            IntPtr positionSwipeMgr, out IntPtr szPositionSwipeData, out int nLength);
        [DllImport("PositionSwipeModule.dll")] extern private static int GetSEIMultiviewDataByChannelCSharp(
            IntPtr positionSwipeMgr, int nChannel, int nMainViewChannel, out IntPtr szPositionSwipeData, out int nLength);
    }
}
