﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace PositionSwipeModuleCSharp
{
    class Program
    {
        //PositionSwipeModuleCSharp 클래스 사용 예시
        static void Main(string[] args)
        {            
            PositionSwipeModuleCSharp positionSwipeModuleCSharp = new PositionSwipeModuleCSharp();

            int nLength = 0;

            int nRet = positionSwipeModuleCSharp.AddPositionSwipeData(
                1, "100001", 3840, 2160, 0, //채널, 카메라ID, 해상도, Position Swipe Length 정보
                -2.8, -8.5, 1917.0, 1356.0, -89.928, 1.0, 192, 108, 3468, 1951, //AdjustData
                866, 1151, 2654, 1101, 3640, 1705, 334, 1865); //4Points Data

            positionSwipeModuleCSharp.AddPositionSwipeData(
                2, "100002", 3840, 2160, nLength, //채널, 카메라ID, 해상도, Position Swipe Length 정보
                -2.8, 9.5, 1917.0, 1338.0, -89.974, 1.043, 192, 108, 3468, 1951, //AdjustData
                668, 1198, 2387, 1076, 3609, 1537, 785, 1914);//4Points Data

            positionSwipeModuleCSharp.AddPositionSwipeData(
                3, "100003", 3840, 2160, nLength, //채널, 카메라ID, 해상도, Position Swipe Length 정보
                4.19, 32.5, 1910.0, 1315.0, -90.812, 1.116, 192, 108, 3468, 1951, //AdjustData
                532, 1233, 2120, 1074, 3462, 1415, 1354, 1854);//4Points Data

            positionSwipeModuleCSharp.AddPositionSwipeData(
                4, "100004", 3840, 2160, nLength, //채널, 카메라ID, 해상도, Position Swipe Length 정보
                -2.2, 26.5, 1912.0, 1321.0, -90.541, 1.165, 192, 108, 3468, 1951, //AdjustData
                475, 1295, 1943, 1087, 3345, 1353, 1763, 1841);//0, 0, 0, 0, 0, 0, 0, 0);//4Points Data

            positionSwipeModuleCSharp.AddPositionSwipeData(
                5, "100005", 3840, 2160, nLength, //채널, 카메라ID, 해상도, Position Swipe Length 정보
                -3.8, 29.5, 1918.0, 1318.0, -88.302, 1.171, 192, 108, 3660 - 192, 2059 - 108,//AdjustData
                408, 1406, 1765, 1088, 3250, 1247, 2177, 1834);//4Points Data

            //int nRet = positionSwipeModuleCSharp.AddPositionSwipeData(
            //    1, "007176", 3840, 2160, 99, //채널, 카메라ID, 해상도, Position Swipe Length 정보
            //    129.9220, 53.2198, 1946.5183, 1058.4911, -89.5767, 0.9602,
            //    1192, 671, 1663, 935, //AdjustData
            //    688, 1036, 2518, 989, 3107, 1082, 840, 1184); //4Points Data

            //positionSwipeModuleCSharp.AddPositionSwipeData(
            //    2, "007177", 3840, 2160, 99, //채널, 카메라ID, 해상도, Position Swipe Length 정보
            //    30.359, -52.597, 2046.0811, 1164.3088, -89.11419, 0.95590,
            //    1192, 671, 1663, 935, //AdjustData
            //    939, 1127, 2840, 1097, 3214, 1207, 733, 1269);//4Points Data

            //positionSwipeModuleCSharp.AddPositionSwipeData(
            //    3, "007178", 3840, 2160, 99, //채널, 카메라ID, 해상도, Position Swipe Length 정보
            //    39.7003, -45.5639, 2036.740056, 1157.2749, -89.262466, 0.9524842,
            //    1192, 671, 1663, 935, //AdjustData
            //    1037, 1107, 2951, 1098, 3170, 1218, 673, 1240);//4Points Data

            //positionSwipeModuleCSharp.AddPositionSwipeData(
            //    4, "007179", 3840, 2160, 99, //채널, 카메라ID, 해상도, Position Swipe Length 정보
            //    -46.1806, -106.07028, 2122.62107, 1217.7813, -90.11276, 0.98057,
            //    1192, 671, 1663, 935, //AdjustData
            //    1371, 1138, 3215, 1191, 3067, 1323, 784, 1246);//4Points Data

            //positionSwipeModuleCSharp.AddPositionSwipeData(
            //    5, "007180", 3840, 2160, 0, //채널, 카메라ID, 해상도, Position Swipe Length 정보
            //    102.19367, -12.46731, 1974.24671, 1124.17834, -90.5489, 1.0,
            //    1192, 671, 1663, 935, //AdjustData
            //    1376, 1036, 3119, 1117, 2753, 1245, 697, 1128);//4Points Data

            positionSwipeModuleCSharp.ApplyPositionSwipeData();

            while (true)
            {
                //AddPositionSwipeData()로 Data들의 입력이 완료된 후 호출
                //BirdView의 채널이 1이라고 가정하고 ROI의 Zoom 배율 및 중심좌표를 입력 후
                //호출하면 나머지 채널(2, 3, 4, 5)의 ROI 중심좌표를 계산
                int nChannel = 1;
                int nZoomRatio = 150;
                int nPosX = 1920;
                int nPosY = 1080;
                nRet = positionSwipeModuleCSharp.SetVecPositionAxis(nChannel, nZoomRatio, nPosX, nPosY);

                //SetVecPositionAxis()로 입력된 채널의 좌표정보를 기준으로 
                //계산된 ROI의 중심좌표를 리턴받는 함수
                //nChannel에 해당하는 Zoom배율, 중심좌표값을 획득
                //UFC에 기획된 Multiview 시나리오에서는 해당 메소드가 필요없을수도 있음
                nChannel = 5;
                nZoomRatio = 0;
                nPosX = 0;
                nPosY = 0;
                nRet = positionSwipeModuleCSharp.GetMovedAxis(nChannel, ref nZoomRatio, ref nPosX, ref nPosY);

                //SetVecPositionAxis()로 입력된 채널의 좌표정보를 기준으로 
                //계산된 ROI의 Data를 arrMultiviewData
                //nChannel에 해당하는 Data이며
                //nChannel 및 획득한 arrMultiviewData를 JSON으로
                //("multiview_roi_channel","multiview_roi_data") 전달
                byte[] arrMultiviewData;
                int nMultiviewDataSize;

                positionSwipeModuleCSharp.GetSEIMultiviewDataByChannel(1, 100, out arrMultiviewData, out nMultiviewDataSize);                
                positionSwipeModuleCSharp.GetSEIMultiviewDataByChannel(2, 100, out arrMultiviewData, out nMultiviewDataSize);
                positionSwipeModuleCSharp.GetSEIMultiviewDataByChannel(3, 100, out arrMultiviewData, out nMultiviewDataSize);
                positionSwipeModuleCSharp.GetSEIMultiviewDataByChannel(4, 100, out arrMultiviewData, out nMultiviewDataSize);
                positionSwipeModuleCSharp.GetSEIMultiviewDataByChannel(5, 100, out arrMultiviewData, out nMultiviewDataSize);
                positionSwipeModuleCSharp.GetSEIPositionSwipeData(out arrMultiviewData, out nMultiviewDataSize);

                for (int i = 0; i < nMultiviewDataSize; i++)
                {
                    Console.WriteLine(arrMultiviewData[i]);
                    //Console.WriteLine(nPosX);
                    //Console.WriteLine(nPosY);
                }
            }
        }
    }
}
