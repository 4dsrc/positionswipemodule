#include "PositionSwipeMgr.h"

PositionSwipeMgr::PositionSwipeMgr()
{
	m_nPositionSwipeDataSize = 0;
	m_nMultiviewDataSize = 0;

	m_szPositionSwipeData = nullptr;
	m_szMultiviewData = nullptr;
	m_szMultiviewDataByChannel = nullptr;
}

PositionSwipeMgr::~PositionSwipeMgr()
{
	delete m_szPositionSwipeData;
}

int PositionSwipeMgr::AddPositionSwipeData(
	int nChannel, string strDscID,
	
	int nWidth, int nHeight, int nPositionSwipeLength,

	double dMoveX, double dMoveY,
	double dRotateX, double dRotateY,
	double dAngle, double dScale,
	int nMarginX,
	int nMarginY,
	int nMarginWidth,
	int nMarginHeight,

	int nX1, int nY1,
	int nX2, int nY2,
	int nX3, int nY3,
	int nX4, int nY4)
{
	StPairKey stPairKey(nChannel, strDscID);
	StPositionSwipeData stPositionSwipeData;

	stPositionSwipeData.stImageInfo.nWidth = nWidth;
	stPositionSwipeData.stImageInfo.nHeight = nHeight;
	stPositionSwipeData.stImageInfo.nPositionSwipeLength = nPositionSwipeLength;

	stPositionSwipeData.stAdjustData.dMoveX = dMoveX;
	stPositionSwipeData.stAdjustData.dMoveY = dMoveY;
	stPositionSwipeData.stAdjustData.dRotateX = dRotateX;
	stPositionSwipeData.stAdjustData.dRotateY = dRotateY;
	stPositionSwipeData.stAdjustData.dAngle = dAngle;
	stPositionSwipeData.stAdjustData.dScale = dScale;

	stPositionSwipeData.stAdjustData.nMarginX = nMarginX;
	stPositionSwipeData.stAdjustData.nMarginY = nMarginY;
	stPositionSwipeData.stAdjustData.nMarginWidth = nMarginWidth;
	stPositionSwipeData.stAdjustData.nMarginHeight = nMarginHeight;

	stPositionSwipeData.stFourPoints.nX1 = nX1;
	stPositionSwipeData.stFourPoints.nY1 = nY1;
	stPositionSwipeData.stFourPoints.nX2 = nX2;
	stPositionSwipeData.stFourPoints.nY2 = nY2;
	stPositionSwipeData.stFourPoints.nX3 = nX3;
	stPositionSwipeData.stFourPoints.nY3 = nY3;
	stPositionSwipeData.stFourPoints.nX4 = nX4;
	stPositionSwipeData.stFourPoints.nY4 = nY4;

	//int nRet = AddData(nChannel, strDscID, stPositionSwipeData);
	LM_RESULT lmRet = AddData(stPairKey, stPositionSwipeData);

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::ApplyPositionSwipeData()
{
	int nRet = 0;
	nRet = convertFourPointsUsingAdjustData();
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	nRet = generateHomographies();
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	return nRet;
}

int PositionSwipeMgr::convertFourPointsUsingAdjustData()
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	int nListSize = 0;
	lmRet = GetListSize(nListSize);
	for (int i = 0; i < nListSize; i++)
	{
		StPositionSwipeData stPositionSwipeData;
		GetDataByIndex(i, stPositionSwipeData);

		StImageInfo stImageInfo = stPositionSwipeData.stImageInfo;
		StAdjustData stAdjustData = stPositionSwipeData.stAdjustData;
		StFourPoints stFourPoints = stPositionSwipeData.stFourPoints;

		if (isFourPointsValid(i) == POSITION_SWIPE_ERROR_INVALID_FOURPOINTS)
			continue;

		Mat rotationMat;
		int nErr = getWarpMat(stPositionSwipeData.stAdjustData, rotationMat);
		if (nErr == POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA)
			continue;

		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX1, stFourPoints.nY1);
		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX2, stFourPoints.nY2);
		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX3, stFourPoints.nY3);
		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX4, stFourPoints.nY4);

		setFourPointsByIndex(i, stFourPoints);
	}

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::generateHomographies()
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	int nRet = 0;
	int nValidFourPoints = 0;
	nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	int nListSize = 0;
	lmRet = GetListSize(nListSize);

	int nIndex1 = nListSize - 1;
	int nIndex2 = 0;

	Mat mHomography;
	StFourPoints stFourPoints1;
	StFourPoints stFourPoints2;

	getFourPointsByIndex(nIndex1, stFourPoints1);
	getFourPointsByIndex(nIndex2, stFourPoints2);

	if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
	{
		while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
		{
			nIndex1--;
			if (nIndex1 < 0)
			{
				lmRet = GetListSize(nListSize);
				nIndex1 = nListSize + nIndex1;
			}
			getFourPointsByIndex(nIndex1, stFourPoints1);
		}
		m_HomographyMgr.CalcHomography(stFourPoints1, stFourPoints2, mHomography);
		addHomographyAtPairIndex(nIndex1, nIndex2, mHomography);
	}

	lmRet = GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
	{
		nIndex1 = i - 1;
		nIndex2 = i;

		getFourPointsByIndex(nIndex1, stFourPoints1);
		getFourPointsByIndex(nIndex2, stFourPoints2);

		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
				{
					lmRet = GetListSize(nListSize);
					nIndex1 = nListSize + nIndex1;
				}
				getFourPointsByIndex(nIndex1, stFourPoints1);
			}
			m_HomographyMgr.CalcHomography(stFourPoints1, stFourPoints2, mHomography);
			addHomographyAtPairIndex(nIndex1, nIndex2, mHomography);
		}		
	}

	return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::recalculatePositionSwipeData(int nIndex, int& nPosX, int& nPosY, int& nPositionLength)
{
	StPositionSwipeData stPositionSwipeData;
	GetDataByIndex(nIndex, stPositionSwipeData);

	Mat rotationMat;
	int nErr = getWarpMat(stPositionSwipeData.stAdjustData, rotationMat);
	if (nErr == POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA)
		return POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA;

	convertSquare(stPositionSwipeData.stImageInfo, stPositionSwipeData.stAdjustData, rotationMat, nPosX, nPosY);
	
	double dbMarginScale = 1.0;
	if (stPositionSwipeData.stAdjustData.nMarginWidth == 0 ||
		stPositionSwipeData.stImageInfo.nWidth == 0)
		dbMarginScale = 1.0;

	dbMarginScale = 
		static_cast<double>(stPositionSwipeData.stImageInfo.nWidth) / 
		static_cast<double>(stPositionSwipeData.stAdjustData.nMarginWidth);
	nPositionLength = static_cast<int>(nPositionLength * dbMarginScale);

	return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::SetVecPositionAxis(int nChannel, int nZoomRatio, int nPosX, int nPosY)
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	int nValidFourPoints = 0;
	int nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	StPairKey stPairKey = StPairKey(nChannel, NULL_STR_KEY);
	AscSortByKey();
	lmRet = ShiftKeyToBegin(stPairKey);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = stImageInfo.nPositionSwipeLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);
	
	int nListSize = 0;
	lmRet = GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;		

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
				{
					lmRet = GetListSize(nListSize);
					nIndex1 = nListSize + nIndex1;
				}
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = stImageInfo.nPositionSwipeLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::GetMovedAxis(int nChannel, int& nZoomRatio, int& nPosX, int& nPosY)
{
	StPairKey stPairKey(nChannel, NULL_STR_KEY);
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByKey(stPairKey, stPositionSwipeData);

	nZoomRatio = stPositionSwipeData.stSwipePoint.nZoomRatio;
	nPosX = static_cast<int>(stPositionSwipeData.stSwipePoint.ptViewPoint.x);
	nPosY = static_cast<int>(stPositionSwipeData.stSwipePoint.ptViewPoint.y);

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::GetMovedAxis(string strDscID, int& nZoomRatio, int& nPosX, int& nPosY)
{
	StPairKey stPairKey(NULL_INT_KEY, strDscID);
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByKey(stPairKey, stPositionSwipeData);

	nZoomRatio = stPositionSwipeData.stSwipePoint.nZoomRatio;
	nPosX = static_cast<int>(stPositionSwipeData.stSwipePoint.ptViewPoint.x);
	nPosY = static_cast<int>(stPositionSwipeData.stSwipePoint.ptViewPoint.y);

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::SetVecPositionAxis(string strDscID, int nZoomRatio, int nPosX, int nPosY)
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	int nValidFourPoints = 0;
	int nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	StPairKey stPairKey = StPairKey(NULL_INT_KEY, strDscID);
	AscSortByKey();
	lmRet = ShiftKeyToBegin(stPairKey);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = stImageInfo.nPositionSwipeLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);

	int nListSize = 0;
	GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
				{
					lmRet = GetListSize(nListSize);
					nIndex1 = nListSize + nIndex1;
				}
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = stImageInfo.nPositionSwipeLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::SetVecPositionAxis(string strDscID, int nPositionLength, int nZoomRatio, int nPosX, int nPosY)
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	int nValidFourPoints = 0;
	int nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	StPairKey stPairKey = StPairKey(NULL_INT_KEY, strDscID);
	AscSortByKey();
	lmRet = ShiftKeyToBegin(stPairKey);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	nRet = recalculatePositionSwipeData(0, nPosX, nPosY, nPositionLength);
	if (nRet != POSITION_SWIPE_OK) return nRet;

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = nPositionLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);

	int nListSize = 0;
	GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
				{
					lmRet = GetListSize(nListSize);
					nIndex1 = nListSize + nIndex1;
				}
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = nPositionLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::SetVecPositionAxis(string strDscID, int nPositionLength, int nZoomRatio, int nPosX, int nPosY, vector<Point2d>& vecLog)
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	int nValidFourPoints = 0;
	int nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	StPairKey stPairKey = StPairKey(NULL_INT_KEY, strDscID);
	AscSortByKey();
	lmRet = ShiftKeyToBegin(stPairKey);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	nRet = recalculatePositionSwipeData(0, nPosX, nPosY, nPositionLength);
	if (nRet != POSITION_SWIPE_OK) return nRet;

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = nPositionLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);
	
	int nListSize = 0;
	GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
				{
					lmRet = GetListSize(nListSize);
					nIndex1 = nListSize + nIndex1;
				}
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = nPositionLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
			
			vecLog.push_back(stSwipePoint.ptViewPoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::GetPositionSwipeLength(string strDscID, int& nPositionLength)
{
	int nListSize = 0;
	GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
	{
		StPairKey stPairKey = StPairKey(NULL_INT_KEY, strDscID);
		GetKeyByIndex(i, stPairKey);
		string strCurrentDscID = stPairKey.strKey;

		if (strCurrentDscID.compare(strDscID) == 0)
		{
			StSwipePoint stSwipePoint;
			getSwipePointByIndex(i, stSwipePoint);

			nPositionLength =
				static_cast<int>(stSwipePoint.ptCenterPoint.y) -
				static_cast<int>(stSwipePoint.ptViewPoint.y);

			return POSITION_SWIPE_OK;
		}
	}
	return POSITION_SWIPE_ERROR_DATA_IS_NOT_EXIST;
}

int PositionSwipeMgr::convertSquare(StImageInfo stImageInfo, StAdjustData stAdjustData, Mat rotationMat, int& nX, int& nY)
{
	int nRet = isAdjustDataValid(stAdjustData);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	double dbMarginScale = 1. /
		((double)(stAdjustData.nMarginWidth) / (double)stImageInfo.nWidth);

	double tempMat_[3] = {
		static_cast<double>(nX),
		static_cast<double>(nY),
		1. };
	Mat tempMat(3, 1, CV_64F, tempMat_);
	Mat resultMat = rotationMat * tempMat;

	nX = static_cast<int>(resultMat.at<double>(0));
	nY = static_cast<int>(resultMat.at<double>(1));

	nX += static_cast<int>(stAdjustData.dMoveX);
	nY += static_cast<int>(stAdjustData.dMoveY);

	int nTempSquareX = nX - stAdjustData.nMarginX;
	int nTempSquarey = nY - stAdjustData.nMarginY;

	nX = static_cast<int>(nTempSquareX * dbMarginScale);
	nY = static_cast<int>(nTempSquarey * dbMarginScale);
	
	return nRet;
}

int PositionSwipeMgr::getWarpMat(StAdjustData stAdjustData, Mat& mWarpMat)
{
	int nRet = isAdjustDataValid(stAdjustData);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	double dScale = stAdjustData.dScale;
	double dAngle = stAdjustData.dAngle;
	Point2d ptRotateCenter = Point2d(stAdjustData.dRotateX, stAdjustData.dRotateY);

	if (dScale == 0)
	{
		dScale = 1.;
		dAngle += -90.;
	}

	dAngle = -1 * (dAngle + 90.);
	mWarpMat = getRotationMatrix2D(ptRotateCenter, dAngle, dScale);

	return nRet;
}

int PositionSwipeMgr::isFourPointsValid(int nIndex)
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	StFourPoints stFourPoints;
	int nRet = getFourPointsByIndex(nIndex, stFourPoints);
	if (nRet != POSITION_SWIPE_OK) nRet;

	if ((stFourPoints.nX1 == -1 && stFourPoints.nY1 == -1) &&
		(stFourPoints.nX2 == -1 && stFourPoints.nY2 == -1) &&
		(stFourPoints.nX3 == -1 && stFourPoints.nY3 == -1) &&
		(stFourPoints.nX4 == -1 && stFourPoints.nY4 == -1))
		return POSITION_SWIPE_ERROR_INVALID_FOURPOINTS;
	else if ((stFourPoints.nX1 == 0 && stFourPoints.nY1 == 0) &&
		(stFourPoints.nX2 == 0 && stFourPoints.nY2 == 0) &&
		(stFourPoints.nX3 == 0 && stFourPoints.nY3 == 0) &&
		(stFourPoints.nX4 == 0 && stFourPoints.nY4 == 0))
		return POSITION_SWIPE_ERROR_INVALID_FOURPOINTS;
	else
		return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::isAdjustDataValid(StAdjustData stAdjustData)
{
	if ((stAdjustData.dMoveX == 0.0 && stAdjustData.dMoveY == 0.0) &&
		(stAdjustData.dRotateX == 0.0 && stAdjustData.dRotateY == 0.0) &&
		(stAdjustData.dAngle == 0.0 && stAdjustData.dScale == 0.0) &&
		(stAdjustData.nMarginX == 0 && stAdjustData.nMarginY == 0) &&
		(stAdjustData.nMarginWidth == 0 && stAdjustData.nMarginHeight == 0))
		return POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA;
	else
		return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::getCountValidFourPointsInList(int& nCount)
{
	LM_RESULT lmRet = CheckValidOfList();
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	nCount = 0;

	int nListSize = 0;
	GetListSize(nListSize);
	for (int i = 1; i < nListSize; i++)
		if (isFourPointsValid(i) == POSITION_SWIPE_OK)
			nCount++;

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::getImageInfoByIndex(int nIndex, StImageInfo& stImageInfo)
{
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	stImageInfo = stPositionSwipeData.stImageInfo;

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::getImageInfoByChannel(int nChannel, StImageInfo& stImageInfo)
{
	StPairKey stPairKey = StPairKey(nChannel, NULL_STR_KEY);

	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByKey(stPairKey, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	stImageInfo = stPositionSwipeData.stImageInfo;

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::setFourPointsByIndex(int nIndex, const StFourPoints& stFourPoints)
{
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	stPositionSwipeData.stFourPoints = stFourPoints;

	StPairKey stPairKey(NULL_INT_KEY, NULL_STR_KEY);
	lmRet = GetKeyByIndex(nIndex, stPairKey);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	lmRet = SetData(stPairKey, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::getFourPointsByIndex(int nIndex, StFourPoints& stFourPoints)
{
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	stFourPoints = stPositionSwipeData.stFourPoints;

	return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::setSwipePointByIndex(int nIndex, const StSwipePoint& stSwipePoint)
{
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	stPositionSwipeData.stSwipePoint = stSwipePoint;

	StPairKey stPairKey(NULL_INT_KEY, NULL_STR_KEY);
	lmRet = GetKeyByIndex(nIndex, stPairKey);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	lmRet = SetData(stPairKey, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::getSwipePointByIndex(int nIndex, StSwipePoint& stSwipePoint)
{
	StPositionSwipeData stPositionSwipeData;
	LM_RESULT lmRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	stSwipePoint = stPositionSwipeData.stSwipePoint;

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::getCenterPointByIndex(int nIndex, Point2d& ptCenterPoint)
{
	LM_RESULT lmRet = CheckValidOfIndex(nIndex);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	StPositionSwipeData stPositionSwipeData;
	lmRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	ptCenterPoint = stPositionSwipeData.stSwipePoint.ptCenterPoint;

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::addHomographyAtPairIndex(int nIndex1, int nIndex2, const Mat& mHomography)
{
	int nRet = 0;
	int nChannel1 = 0, nChannel2 = 0;

	StPairKey stPairKey1(NULL_INT_KEY, NULL_STR_KEY);
	LM_RESULT lmRet = GetKeyByIndex(nIndex1, stPairKey1);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	StPairKey stPairKey2(NULL_INT_KEY, NULL_STR_KEY);
	lmRet = GetKeyByIndex(nIndex2, stPairKey2);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	nRet = m_HomographyMgr.AddHomographyAtPairChannel(stPairKey1.nKey, stPairKey2.nKey, mHomography);

	return ErrorReturn(lmRet);
}

int PositionSwipeMgr::getHomographyAtPairIndex(int nIndex1, int nIndex2, Mat& mHomography)
{
	int nRet = 0;
	int nChannel1 = 0, nChannel2 = 0;

	StPairKey stPairKey1(NULL_INT_KEY, NULL_STR_KEY);
	LM_RESULT lmRet = GetKeyByIndex(nIndex1, stPairKey1);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	StPairKey stPairKey2(NULL_INT_KEY, NULL_STR_KEY);
	lmRet = GetKeyByIndex(nIndex2, stPairKey2);
	if (lmRet != LM_RESULT::OK) return ErrorReturn(lmRet);

	nRet = m_HomographyMgr.GetHomographyAtPairChannel(stPairKey1.nKey, stPairKey2.nKey, mHomography);

	return nRet;
}

void PositionSwipeMgr::GeneratePositionSwipePayload()
{
	m_nPositionSwipeDataSize = 0;

	if (m_szPositionSwipeData != nullptr)
		free(m_szPositionSwipeData);

	int nListSize = 0;
	GetListSize(nListSize);
	unsigned char* szPositionSwipeData = nullptr;
	szPositionSwipeData = (unsigned char*)malloc(sizeof(short int) * (4 + 9 * nListSize));

	if (nListSize <= 0)
		return;

	StImageInfo stImageInfo;
	getImageInfoByIndex(nListSize - 1, stImageInfo);

	short int nWidth = static_cast<short int>(stImageInfo.nWidth);
	short int nHeight = static_cast<short int>(stImageInfo.nHeight);
	short int nPositionLength = static_cast<short int>(stImageInfo.nPositionSwipeLength);
	short int nCount = static_cast<short int>(nListSize);

	nWidth = htons(nWidth);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nWidth, sizeof(nWidth));
	m_nPositionSwipeDataSize += 2;

	nHeight = htons(nHeight);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nHeight, sizeof(nHeight));
	m_nPositionSwipeDataSize += 2;

	nPositionLength = htons(nPositionLength);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nPositionLength, sizeof(nPositionLength));
	m_nPositionSwipeDataSize += 2;

	nCount = htons(nCount);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nCount, sizeof(nCount));
	m_nPositionSwipeDataSize += 2;

	for (int i = 0; i < nListSize; i++)
	{
		int nRet = isFourPointsValid(i);
		if (nRet == POSITION_SWIPE_ERROR_INVALID_FOURPOINTS) continue;

		StFourPoints stFourPoints;
		getFourPointsByIndex(i, stFourPoints);
		
		int nChannel = 0;
		StPairKey stPairKey(NULL_INT_KEY, NULL_STR_KEY);
		GetKeyByIndex(i, stPairKey);
		nChannel = stPairKey.nKey;

		short int nValue = static_cast<short int>(nChannel);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX1);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY1);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX2);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY2);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX3);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY3);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX4);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY4);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;
	}

	int nByteSize = m_nPositionSwipeDataSize;
	unsigned char* szTemp = rbsp2ebsp(&m_nPositionSwipeDataSize, szPositionSwipeData, nByteSize);

	free(szPositionSwipeData);

	m_szPositionSwipeData = szTemp;
}

int PositionSwipeMgr::GenerateMultiviewPayloadByChannel(int nChannel)
{
	m_nMultiviewDataSizeByChannel = 0;

	if (m_szMultiviewDataByChannel != nullptr)
		free(m_szMultiviewDataByChannel);

	const int nMultiviewPayloadSize = 26;

	unsigned char* szMultiviewDataByChannel = nullptr;
	szMultiviewDataByChannel = (unsigned char*)malloc(
		nMultiviewPayloadSize);

	StImageInfo stImageInfo;
	int nRet = getImageInfoByChannel(nChannel, stImageInfo);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	short int nWidth = static_cast<short int>(stImageInfo.nWidth);
	short int nHeight = static_cast<short int>(stImageInfo.nHeight);

	nWidth = htons(nWidth);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nWidth, sizeof(nWidth));
	m_nMultiviewDataSizeByChannel += 2;

	nHeight = htons(nHeight);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nHeight, sizeof(nHeight));
	m_nMultiviewDataSizeByChannel += 2;

	int nZoomRatio = 0;
	int nPosX = 0;
	int nPosY = 0;
	nRet = GetMovedAxis(nChannel, nZoomRatio, nPosX, nPosY);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	short int nValue = static_cast<short int>(nZoomRatio);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nPosX);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nPosY);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	int nMarginPadding = 34952;

	for (int i = 0; i < 8; i++)
	{
		//Margin Padding
		nValue = static_cast<short int>(nMarginPadding);
		nValue = htons(nValue);
		::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
		m_nMultiviewDataSizeByChannel += 2;
	}

	int nByteSize = m_nMultiviewDataSizeByChannel;
	unsigned char* szTemp = rbsp2ebsp(&m_nMultiviewDataSizeByChannel, szMultiviewDataByChannel, nByteSize);

	free(szMultiviewDataByChannel);

	m_szMultiviewDataByChannel = szTemp;

	return nRet;
}

int PositionSwipeMgr::GenerateMultiviewPayloadByChannel(int nChannel, int nMainViewChannel)
{
	m_nMultiviewDataSizeByChannel = 0;

	if (m_szMultiviewDataByChannel != nullptr)
		free(m_szMultiviewDataByChannel);

	const int nMultiviewPayloadSize = 26;

	unsigned char* szMultiviewDataByChannel = nullptr;
	szMultiviewDataByChannel = (unsigned char*)malloc(
		nMultiviewPayloadSize);

	StImageInfo stImageInfo;
	int nRet = getImageInfoByChannel(nChannel, stImageInfo);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	short int nWidth = static_cast<short int>(stImageInfo.nWidth);
	short int nHeight = static_cast<short int>(stImageInfo.nHeight);

	nWidth = htons(nWidth);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nWidth, sizeof(nWidth));
	m_nMultiviewDataSizeByChannel += 2;

	nHeight = htons(nHeight);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nHeight, sizeof(nHeight));
	m_nMultiviewDataSizeByChannel += 2;

	int nZoomRatio = 0;
	int nPosX = 0;
	int nPosY = 0;
	nRet = GetMovedAxis(nChannel, nZoomRatio, nPosX, nPosY);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	short int nValue = static_cast<short int>(nZoomRatio);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nPosX);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nPosY);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nMainViewChannel);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	int nMarginPadding = 34952;

	for (int i = 0; i < 7; i++)
	{
		//Margin Padding
		nValue = static_cast<short int>(nMarginPadding);
		nValue = htons(nValue);
		::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
		m_nMultiviewDataSizeByChannel += 2;
	}

	int nByteSize = m_nMultiviewDataSizeByChannel;
	unsigned char* szTemp = rbsp2ebsp(&m_nMultiviewDataSizeByChannel, szMultiviewDataByChannel, nByteSize);

	free(szMultiviewDataByChannel);

	m_szMultiviewDataByChannel = szTemp;

	return nRet;
}

int PositionSwipeMgr::GetPositionSwipeDataForSEI(unsigned char*& szPositionSwipeData, int& nLength)
{
	GeneratePositionSwipePayload();

	szPositionSwipeData = m_szPositionSwipeData;
	nLength = m_nPositionSwipeDataSize;

	return 1;
}

int PositionSwipeMgr::GetSEIMultiviewDataByChannel(int nChannel, unsigned char*& szMultiviewData, int& nLength)
{
	int nRet = GenerateMultiviewPayloadByChannel(nChannel);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	szMultiviewData = m_szMultiviewDataByChannel;
	nLength = m_nMultiviewDataSizeByChannel;

	return nRet;
}

int PositionSwipeMgr::GetSEIMultiviewDataByChannel(int nChannel, int nMainViewChannel, unsigned char*& szMultiviewData, int& nLength)
{
	int nRet = GenerateMultiviewPayloadByChannel(nChannel, nMainViewChannel);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	szMultiviewData = m_szMultiviewDataByChannel;
	nLength = m_nMultiviewDataSizeByChannel;

	return nRet;
}

unsigned char* PositionSwipeMgr::rbsp2ebsp(int* ebsp_size, unsigned char* rbsp, int rbsp_size)
{
	unsigned char* ebsp = (unsigned char*)malloc(sizeof(unsigned char) * rbsp_size);
	int j = 0;
	int count = 0;

	for (int i = 0; i < rbsp_size; i++) {
		if (count >= 2 && !(rbsp[i] & 0xFC)) {
			j++;
			//realloc(ebsp, rbsp_size + j - i);
			ebsp = (unsigned char*)realloc(ebsp, rbsp_size + j - i);
			ebsp[j - 1] = 0x03;

			count = 0;
		}
		ebsp[j] = rbsp[i];

		if (rbsp[i] == 0x00)
			count++;
		else
			count = 0;
		j++;
	}
	*ebsp_size = j;

	return ebsp;
}

string PositionSwipeMgr::GetVersion()
{
	return POSITION_TRACKING_MODULE_VERSION;
}

int PositionSwipeMgr::ErrorReturn(LM_RESULT lmErrCode)
{
	if (lmErrCode == LM_RESULT::ERROR_DATA_IS_NOT_EXIST)
		return POSITION_SWIPE_ERROR_DATA_IS_NOT_EXIST;
	else if (lmErrCode == LM_RESULT::ERROR_DATA_ALREADY_EXIST)
		return POSITION_SWIPE_ERROR_DATA_ALREADY_EXIST;
	else if (lmErrCode == LM_RESULT::ERROR_LIST_IS_EMPTY)
		return POSITION_SWIPE_ERROR_LIST_IS_EMPTY;
	else if (lmErrCode == LM_RESULT::ERROR_LIST_IS_NOT_VALID)
		return POSITION_SWIPE_ERROR_LIST_IS_NOT_VALID;
	else if (lmErrCode == LM_RESULT::OK)
		return POSITION_SWIPE_OK;
	else
		return POSITION_SWIPE_OK;
}